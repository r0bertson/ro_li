module bitbucket.org/r0bertson/ro_li/frontend

go 1.13

require (
	bitbucket.org/r0bertson/ro_li/backend v0.0.0-20200812032701-d2674902ab76
	github.com/rs/zerolog v1.19.0
	golang.org/x/oauth2 v0.0.0-20190226205417-e64efc72b421
)
