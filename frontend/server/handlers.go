package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/client"
	"html/template"
	"log"
	"net/http"
	"time"
)

func (s *Server) profile(w http.ResponseWriter, r *http.Request) {

	// prevent browsers from caching page content, so pushing back button
	// on browser won't show user's data after session expired or logout
	w.Header().Set("Cache-Control", "no-store, must-revalidate")

	//if session cookie doesn't exists, redirect to login page
	cookie, err := r.Cookie("roli_session")
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}

	if r.Method == "GET" {
		//getting user profile
		user, err := s.Client.GetMe(cookie)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		//verify if profile is setup (it'll only happen on the first time)
		if user.IsSetup {
			http.Redirect(w, r, "profile-edit", http.StatusFound)
			return
		}

		render(w, "templates/profile.html", user)
	} else {
		http.Error(w, "page not found", http.StatusInternalServerError)
		return
	}
}

func (s *Server) profileEdit(w http.ResponseWriter, r *http.Request) {
	// prevent browsers from caching page content, so pushing back button
	// on browser won't show user's data after session expired or logout
	w.Header().Set("Cache-Control", "no-store, must-revalidate")

	//if session cookie doesn't exists, redirect to login page
	cookie, err := r.Cookie("roli_session")
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
	if r.Method == "GET" {
		//getting user profile
		user, err := s.Client.GetMe(cookie)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		//build form struct (with errors!)
		msg := ProfileMessage{
			IsSetup:   user.IsSetup,
			Type:      user.Type,
			Username:  user.Username,
			FullName:  user.FullName,
			Address:   user.Address,
			Telephone: user.Telephone,
			Errors:    nil,
		}
		render(w, "templates/edit_profile.html", msg)
	} else {
		r.ParseForm()
		msg := ProfileMessage{
			Type:      r.PostFormValue("type"),
			Username:  r.PostFormValue("username"),
			FullName:  r.PostFormValue("fullname"),
			Address:   r.PostFormValue("address"),
			Telephone: r.PostFormValue("telephone"),
			Errors:    nil,
		}

		if !msg.IsValid() {
			render(w, "templates/edit_profile.html", msg)
			return
		}

		cookie, err := r.Cookie("roli_session")
		if err != nil {
			http.Redirect(w, r, "login", http.StatusFound)
		}

		_, err = s.Client.UpdateUser(msg.Username, msg.FullName, msg.Address, msg.Telephone, cookie)
		if err != nil {
			if err, ok := err.(*client.ErrorClient); ok {
				if err.Code != http.StatusInternalServerError {
					msg.Errors["Generic"] = err.Error()
					render(w, "templates/edit_profile.html", msg)
					return
				}
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/profile", http.StatusFound)
	}
}

func (s *Server) login(w http.ResponseWriter, r *http.Request) {

	if r.Method == "GET" {
		//if session cookie exists, redirect to profile page
		if _, err := r.Cookie("roli_session"); err == nil {
			http.Redirect(w, r, "/profile", http.StatusFound)
		}
		msg := LoginMessage{GoogleLink: s.GoogleOAuth.AuthCodeURL(NewID(32))}
		render(w, "templates/login.html", msg)
	} else {
		r.ParseForm()
		msg := LoginMessage{
			Username: r.PostFormValue("username"),
			Password: r.PostFormValue("password"),
			Errors:   nil,
		}

		if !msg.IsValid() {
			render(w, "templates/login.html", msg)
			return
		}

		cookie, user, err := s.Client.Login(msg.Username, msg.Password)
		if err != nil {
			if err, ok := err.(*client.ErrorClient); ok {
				if err.Code != http.StatusInternalServerError {
					msg.Errors["Generic"] = err.Error()
					render(w, "templates/login.html", msg)
					return
				}
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.SetCookie(w, cookie)
		if user.IsSetup {
			http.Redirect(w, r, "/profile-edit", http.StatusFound)
			return
		}

		http.Redirect(w, r, "/profile", http.StatusFound)
	}
}

func (s *Server) resetPassword(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		token, ok := r.URL.Query()["token"]
		if !ok || len(token[0]) < 1 {
			http.Error(w, "token is missing", http.StatusNotFound)
			return
		}
		render(w, "templates/reset-password.html", ResetPasswordMessage{Token: token[0]})
	} else {
		r.ParseForm()
		msg := ResetPasswordMessage{
			Token:     r.PostFormValue("token"), //storing token inside form to avoid user changing it on URL
			Password:  r.PostFormValue("password"),
			Password2: r.PostFormValue("password2"),
			Errors:    nil,
		}

		if !msg.IsValid() {
			render(w, "templates/reset-password.html", msg)
			return
		}

		if err := s.Client.SubmitPasswordReset(msg.Token, msg.Password); err != nil {
			if err, ok := err.(*client.ErrorClient); ok {
				if err.Code != http.StatusInternalServerError {
					msg.Errors["Generic"] = err.Error()
					render(w, "templates/reset-password.html", msg)
					return
				}
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "/login", http.StatusFound)
		return
	}
}

func (s *Server) forgot(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		render(w, "templates/forgot.html", nil)
	} else {
		r.ParseForm()
		msg := ForgotMessage{
			Username: r.PostFormValue("username"),
			Errors:   nil,
		}

		if !msg.IsValid() {
			render(w, "templates/forgot.html", msg)
			return
		}

		if err := s.Client.RequestPasswordReset(msg.Username); err != nil {
			if err, ok := err.(*client.ErrorClient); ok {
				if err.Code != http.StatusInternalServerError {
					msg.Errors["Generic"] = err.Error()
					render(w, "templates/forgot.html", msg)
					return
				}
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		msg.Success = true
		render(w, "templates/forgot.html", msg)
		return
	}
}
func (s *Server) getGoogleLink(w http.ResponseWriter) string {
	randomState := NewID(32)

	http.SetCookie(w, &http.Cookie{
		Name:     "google_state",
		Value:    randomState,
		MaxAge:   int(60 * time.Second),
		Secure:   false, //TODO:USE HTTPS!,
		HttpOnly: true,
		Path:     "/",
	})

	return s.GoogleOAuth.AuthCodeURL(randomState)
}
func (s *Server) signup(w http.ResponseWriter, r *http.Request) {
	//if session cookie exists, redirect to profile page
	if _, err := r.Cookie("roli_session"); err == nil {
		http.Redirect(w, r, "/profile", http.StatusFound)
	}

	if r.Method == "GET" {
		msg := SignUpMessage{
			GoogleLink: s.getGoogleLink(w),
		}
		t, _ := template.ParseFiles("templates/signup.html")
		t.Execute(w, msg)
	} else {
		r.ParseForm()
		msg := SignUpMessage{
			GoogleLink: s.getGoogleLink(w),
			Username:   r.PostFormValue("username"),
			Password:   r.PostFormValue("password"),
			Password2:  r.PostFormValue("password2"),
			Errors:     nil,
		}

		if !msg.IsValid() {
			render(w, "templates/signup.html", msg)
			return
		}

		_, err := s.Client.CreateUser(msg.Username, msg.Password)
		if err != nil {
			if err, ok := err.(*client.ErrorClient); ok {
				if err.Code != http.StatusInternalServerError {
					msg.Errors["Generic"] = err.Error()
					render(w, "templates/signup.html", msg)
					return
				}
			}
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		http.Redirect(w, r, "login", http.StatusFound)
	}
}

func (s *Server) handleGoogle(w http.ResponseWriter, r *http.Request) {
	code, ok := r.URL.Query()["code"]
	if !ok || len(code[0]) < 1 {
		http.Error(w, "code is missing", http.StatusBadRequest)
		return
	}

	cookie, usr, err := s.Client.HandleGoogleAuth(code[0])
	if err != nil {
		if err, ok := err.(*client.ErrorClient); ok {
			if err.Code == http.StatusBadRequest {
				render(w, "templates/login.html", LoginMessage{
					GoogleLink: s.GoogleOAuth.AuthCodeURL(NewID(32)),
					Errors: map[string]string{"Generic":err.Error()},
				})
				return
			}
		}
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.SetCookie(w, cookie)
	if usr.IsSetup {
		http.Redirect(w, r, "/profile-edit", http.StatusFound)
	}

	http.Redirect(w, r, "/profile", http.StatusFound)

}

func (s *Server) logout(w http.ResponseWriter, r *http.Request) {

	//if session cookie is missing, redirect to login page
	cookie, err := r.Cookie("roli_session")
	if err != nil {
		http.Redirect(w, r, "login", http.StatusFound)
		return
	}

	expired, err := s.Client.Logout(cookie)
	if err != nil {
		http.Redirect(w, r, "profile", http.StatusInternalServerError)
		return
	}

	http.SetCookie(w, expired) //override session cookie with an expired one

	http.Redirect(w, r, "login", http.StatusFound)
}

func render(w http.ResponseWriter, filename string, data interface{}) {
	tmpl, err := template.ParseFiles(filename)
	if err != nil {
		log.Println(err)
		http.Error(w, "Oops, something went wrong", http.StatusInternalServerError)
	}

	if err := tmpl.Execute(w, data); err != nil {
		log.Println(err)
		http.Error(w, "Oops, something went wrong", http.StatusInternalServerError)
	}
}
