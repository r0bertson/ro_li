package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/client"
	"context"
	"fmt"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

// Config contains the configuration information needed to start the blob service.
type Config struct {
	AppName                string
	Port                   int    `env:"PORT,default=8080"`
	BackendURL             string `env:"BACKEND_URL,required"`
	GoogleOAuthClientID    string `env:"GOOGLE_OAUTH_CLIENT_ID,required"`
	GoogleOAuthSecret      string `env:"GOOGLE_OAUTH_SECRET,required"`
	GoogleOAuthRedirectURL string `env:"GOOGLE_OAUTH_REDIRECT_URL,required"`
}

// Server is the server structure.
type Server struct {
	ApplicationName string
	Ctx             context.Context
	Port            int
	Client          *client.RESTClient
	GoogleOAuth     *oauth2.Config
}

// NewServer creates the server structure.
func NewServer(cfg *Config) *Server {

	/* DESIGN DECISION
	Instead of building requests and decoding their respective responses manually inside frontend's
	handlers, I built a backend SDK to avoid too much code on the front. It is out of scope, but I helps me
	focus on the separation between frontend and backend.
	*/
	s := &Server{
		ApplicationName: cfg.AppName,
		Port:            cfg.Port,
		Client:          client.New(cfg.BackendURL),
		Ctx:             context.Background(),
	}

	s.GoogleOAuth = &oauth2.Config{
		ClientID:     cfg.GoogleOAuthClientID,
		ClientSecret: cfg.GoogleOAuthSecret,
		RedirectURL:  cfg.GoogleOAuthRedirectURL,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}

	return s
}

// Serve runs a server event loop and performs shutdown when application is stopped.
func (s *Server) Serve() {
	errChan := make(chan error, 0)
	go func() {
		fs := http.FileServer(http.Dir("templates"))
		http.Handle("/css/", fs)
		//setting handlers
		http.HandleFunc("/signup", s.signup)
		http.HandleFunc("/", s.login)
		http.HandleFunc("/login", s.login)
		http.HandleFunc("/logout", s.logout)
		http.HandleFunc("/profile", s.profile)
		http.HandleFunc("/profile-edit", s.profileEdit)

		//handles redirection by google
		http.HandleFunc("/signup/google", s.handleGoogle)

		http.HandleFunc("/forgot", s.forgot)
		http.HandleFunc("/reset-password", s.resetPassword)

		log.Info().Int("port", s.Port).Msg("server started")
		err := http.ListenAndServe(fmt.Sprintf(":%d", s.Port), nil)
		if err != nil {
			errChan <- err
		}
	}()

	signalCh := make(chan os.Signal, 0)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)

	var err error

	select {
	case <-signalCh:
	case err = <-errChan:
	}

	if err == nil {
		log.Info().Msg("server shutting down")
	} else {
		log.Fatal().Err(err).Msg("server failed an will shutdown")
	}
}
