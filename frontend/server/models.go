package server

import (
	"regexp"
	"strings"
)

type LoginMessage struct {
	GoogleLink string
	Username   string
	Password   string
	Errors     map[string]string
}

type SignUpMessage struct {
	GoogleLink string
	Username   string
	Password   string
	Password2  string
	Errors     map[string]string
}

type ProfileMessage struct {
	IsSetup   bool
	Type      string
	Username  string
	FullName  string
	Address   string
	Telephone string
	Errors    map[string]string
}

type ForgotMessage struct {
	Username string
	Success  bool
	Errors   map[string]string
}
type ResetPasswordMessage struct {
	Token     string
	Password  string
	Password2 string
	Errors    map[string]string
}

//source: https://www.w3.org/TR/2016/REC-html51-20161101/sec-forms.html#email-state-typeemail
var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (m *LoginMessage) IsValid() bool {
	m.Errors = make(map[string]string)

	match := emailRegex.Match([]byte(strings.TrimSpace(m.Username)))
	if match == false {
		m.Errors["Username"] = "Please enter a valid email address"
	}

	if m.Password == "" {
		m.Errors["Password"] = "Please enter a password"
	}

	return len(m.Errors) == 0

}

func (m *SignUpMessage) IsValid() bool {
	m.Errors = make(map[string]string)

	match := emailRegex.Match([]byte(strings.TrimSpace(m.Username)))
	if match == false {
		m.Errors["Username"] = "Please enter a valid email address"
	}

	if m.Password == "" {
		m.Errors["Password"] = "Please enter a password"
	}

	if m.Password2 == "" {
		m.Errors["Password2"] = "Please enter a password confirmation"
	}

	if m.Password2 != m.Password {
		m.Errors["Password2"] = "Passwords are different"
	}

	return len(m.Errors) == 0
}

func (m *ProfileMessage) IsValid() bool {
	m.Errors = make(map[string]string)

	if m.Type == "native" {
		match := emailRegex.Match([]byte(strings.TrimSpace(m.Username)))
		if match == false {
			m.Errors["Username"] = "Please enter a valid email address"
		}
	}

	if m.FullName == "" {
		m.Errors["FullName"] = "Please enter your full name"
	}

	if m.Address == "" {
		m.Errors["Address"] = "Please enter an address"
	}

	if m.Telephone == "" {
		m.Errors["Telephone"] = "Please enter your telephone number"
	}

	return len(m.Errors) == 0
}

func (m *ForgotMessage) IsValid() bool {
	m.Errors = make(map[string]string)

	match := emailRegex.Match([]byte(strings.TrimSpace(m.Username)))
	if match == false {
		m.Errors["Username"] = "Please enter a valid email address"
	}

	return len(m.Errors) == 0
}

func (m *ResetPasswordMessage) IsValid() bool {
	m.Errors = make(map[string]string)

	if m.Password == "" {
		m.Errors["Password"] = "Please enter a password"
	}

	if m.Password2 == "" {
		m.Errors["Password2"] = "Please enter a password confirmation"
	}

	if m.Password2 != m.Password {
		m.Errors["Password2"] = "Passwords are different"
	}

	return len(m.Errors) == 0
}
