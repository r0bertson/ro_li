package main

import (
	"bitbucket.org/r0bertson/ro_li/backend/server"
	"github.com/joeshaw/envdecode"

	"github.com/rs/zerolog/log"
)

const appName = "ro_li-backend"

func main() {
	cfg := server.Config{AppName: appName}

	/* DESIGN DECISION
	using envdecode to avoid repetition, but the same can be easily
	achieved with multiple os.Getenv(key) and ordinary error handling */
	if err := envdecode.StrictDecode(&cfg); err != nil {
		log.Fatal().Err(err).Msg("failed to process environment variables")
	}

	s := server.NewServer(&cfg)
	s.Serve()
}
