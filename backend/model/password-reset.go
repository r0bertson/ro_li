package model

import "time"

type PasswordReset struct {
	Token     string
	UserID    string
	ExpiresAt time.Time
}
