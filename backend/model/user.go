package model

import (
	"database/sql"
	"encoding/json"
	"errors"
	"regexp"
)

type User struct {
	ID         string  `json:"id"`
	Username   string  `json:"username"`
	Type       string  `json:"type"`
	Password   sql.NullString `json:"-"`
	ExternalID sql.NullString `json:"external_id"`
	FullName   sql.NullString  `json:"full_name"`
	Address    sql.NullString  `json:"address"`
	Telephone  sql.NullString  `json:"telephone"`
	IsSetup    bool    `json:"is_setup"`
}

type UserPublicView struct {
	ID         string  `json:"id"`
	Username   string  `json:"username"`
	Type       string  `json:"type"`
	FullName   string  `json:"full_name"`
	Address    string  `json:"address"`
	Telephone  string  `json:"telephone"`
	IsSetup    bool    `json:"is_setup"`
}

func (u *User) GetPublicView() UserPublicView {
	return UserPublicView{
		ID:       u.ID,
		Username:  u.Username,
		Type:      u.Type,
		FullName:  u.FullName.String,
		Address:   u.Address.String,
		Telephone: u.Telephone.String,
		IsSetup:   u.IsSetup,
	}
}
//source: https://www.w3.org/TR/2016/REC-html51-20161101/sec-forms.html#email-state-typeemail
var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (u *User) Patch(patch []byte) error {
	updates := map[string]interface{}{}
	err := json.Unmarshal(patch, &updates)
	if err != nil {
		return errors.New("unmarshaling patch request: " + err.Error())
	}
	roFields := map[string]string{
		"id":          "ID",
		"type":        "type",
		"is_setup":    "is_setup",
		"password":    "password",
		"external_id": "external_id",
	}
	for fld, label := range roFields {
		if _, ok := updates[fld]; ok {
			return errors.New("can't patch user's field " + label)
		}
	}
	/**
	TODO
		- ENHANCEMENT
		add schema validations to validate telephone number
	*/
	if err = StringField(&u.Username, updates, "username"); err != nil {
		return err
	}

	//username must be an email
	if !ValidateEmail(u.Username) {
		return errors.New("invalid username/email")
	}

	if err = NullStringField(&u.FullName, updates, "full_name"); err != nil {
		return err
	}
	if err = NullStringField(&u.Address, updates, "address"); err != nil {
		return err
	}
	if err = NullStringField(&u.Telephone, updates, "telephone"); err != nil {
		return err
	}

	return nil
}

// Process a string field.
func StringField(dst *string, fields map[string]interface{}, key string) error {
	val, ok := fields[key]
	if !ok {
		return nil
	}
	*dst, ok = val.(string)
	if !ok {
		return errors.New("invalid value for '" + key + "': not a string")
	}
	delete(fields, key)
	return nil
}

func NullStringField(dst *sql.NullString, fields map[string]interface{}, key string) error {
	val, ok := fields[key]
	if !ok {
		return nil
	}
	stringVal, ok := val.(string)
	dst.Valid = true
	if !ok {
		return errors.New("invalid value for '" + key + "': not a string")
	}
	*dst = sql.NullString{String: stringVal, Valid: true}
	delete(fields, key)
	return nil
}

// ValidateEmail checks if the email provided passes the required structure and length.
//TODO: there are faster ways to do this
func ValidateEmail(email string) bool {
	if len(email) < 3 && len(email) > 254 {
		return false
	}
	return emailRegex.MatchString(email)
}
