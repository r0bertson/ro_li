package model


type RespLogin struct {
	UserID string `json:"user_id"`
	Token  string `json:"token"`
}
