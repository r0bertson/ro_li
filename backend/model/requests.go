package model

type ReqCreateUser struct {
	Type       string  `json:"type"`
	Username   string  `json:"username"`
	Password   *string `json:"password"`
	ExternalID *string `json:"external_id"`
}

type ReqLoginNative struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type ReqGoogleAuth struct {
	Code string `json:"code"`
}

type ReqResetPassword struct {
	Token    string `json:"token"`
	Password string `json:"password"`
}
