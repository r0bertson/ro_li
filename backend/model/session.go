package model

// Session holds the information that associates session cookies with users.
type Session struct {
	Token  string `db:"token"`
	UserID string `db:"user_id"`
	Email  string `db:"email"`
}