package client

import "bitbucket.org/r0bertson/ro_li/backend/model"

type Client interface {
	Login(username string, password string) (*model.User, error)
}