package client

import (
	"bitbucket.org/r0bertson/ro_li/backend/model"
	serverUtils "bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

// ErrorClient implements error interface se we can return additional fields, such as status code
// case code is 0, it means something went wrong while parsing body from response. that's very
// unlikely to happen, but can be handled by whoever consumes this client
type ErrorClient struct {
	err string
	Code int
}

func (e *ErrorClient) Error() string {
	return e.err
}


type RESTClient struct {
	baseURL string
}
// New creates a new RESTClient.
func New(baseURL string) *RESTClient {
	return &RESTClient{
		baseURL: baseURL,
	}
}

// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) Login(username string, password string) (*http.Cookie, *model.UserPublicView, error) {
	// Encode JSON request body.
	req := model.ReqLoginNative{
		Username: username,
		Password: password,
	}

	body, err := json.Marshal(req)
	if err != nil {
		return nil, nil, err
	}

	rsp, err := http.Post(c.baseURL+"/auth/login", "application/json", bytes.NewBuffer(body))
	if err != nil {
		return nil, nil, err
	}

	// Decode response.
	if rsp.StatusCode == http.StatusOK {
		resp := model.UserPublicView{}
		rspBody, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			return nil, nil, err
		}
		defer rsp.Body.Close()

		if err = json.Unmarshal(rspBody, &resp); err != nil {
			return nil, nil, err
		}
		for _, cookie := range rsp.Cookies() {
			if cookie.Name == "roli_session" {
				return cookie, &resp, nil
			}
		}
		return nil, nil, errors.New("missing session cookie")
	}

	return nil, nil, BuildErrorFromErrMsg(rsp)
	//return nil, BuildErrorFromErrMsg(rsp)
}



// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) HandleGoogleAuth(code string) (*http.Cookie, *model.UserPublicView, error) {
	// Encode JSON request body.
	req := model.ReqGoogleAuth{
		Code:code,
	}

	body, err := json.Marshal(req)
	if err != nil {
		return nil, nil, err
	}

	rsp, err := http.Post(c.baseURL+"/auth/google", "application/json", bytes.NewBuffer(body))
	if err != nil {
		return nil, nil, err
	}

	// Decode response.
	if rsp.StatusCode == http.StatusOK {
		resp := model.UserPublicView{}
		rspBody, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			return nil, nil, err
		}
		defer rsp.Body.Close()

		if err = json.Unmarshal(rspBody, &resp); err != nil {
			return nil, nil, err
		}
		for _, cookie := range rsp.Cookies() {
			if cookie.Name == "roli_session" {
				return cookie, &resp, nil
			}
		}
		return nil, nil, errors.New("missing session cookie")
	}

	return nil, nil, BuildErrorFromErrMsg(rsp)
}



// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) GetMe(cookie *http.Cookie) (*model.UserPublicView, error) {
	req, err := http.NewRequest("GET", c.baseURL + "/me", nil)
	if err != nil {
		return nil, err
	}
	req.AddCookie(cookie)
	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	// Decode response.
	if rsp.StatusCode == http.StatusOK {
		resp := model.UserPublicView{}
		rspBody, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			return nil, err
		}
		defer rsp.Body.Close()

		if err = json.Unmarshal(rspBody, &resp); err != nil {
			return nil, err
		}

		return &resp, nil
	}

	return nil, BuildErrorFromErrMsg(rsp)
}



// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) CreateUser(username, password string) (*model.User, error) {
	// Encode JSON request body.
	reqBody := model.ReqCreateUser{ Type: "native",Username: username, Password: &password, }
	body, err := json.Marshal(reqBody)
	if err != nil {
		return nil, err
	}
	rsp, err := http.Post(c.baseURL+"/users", "application/json", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	// Decode response.
	if rsp.StatusCode == http.StatusOK {
		resp := model.User{}
		rspBody, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			return nil, err
		}
		defer rsp.Body.Close()

		if err = json.Unmarshal(rspBody, &resp); err != nil {
			return nil, err
		}

		return &resp, nil
	}

	return nil, BuildErrorFromErrMsg(rsp)
}



// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) RequestPasswordReset(username string) error {

	url := c.baseURL + "/request-password-reset?username=" + username
	rsp, err := http.Post(url, "application/json", nil)
	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusNoContent {
		return BuildErrorFromErrMsg(rsp)
	}

	return nil
}

func (c *RESTClient) SubmitPasswordReset(token, password string) error {
	reqBody:= model.ReqResetPassword{token, password}
	body, err := json.Marshal(reqBody)
	if err != nil {
		return err
	}
	rsp, err := http.Post(c.baseURL + "/password-reset", "application/json",  bytes.NewBuffer(body))
	if err != nil {
		return err
	}

	if rsp.StatusCode != http.StatusNoContent {
		return BuildErrorFromErrMsg(rsp)
	}

	return nil
}



// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) UpdateUser(username, fullName, address, telephone string, cookie *http.Cookie) (*model.User, error) {
	// building request body.
	payload := map[string]interface{}{}
	payload["username"] = username
	payload["full_name"] = fullName
	payload["address"] = address
	payload["telephone"] = telephone

	body, err := json.Marshal(payload)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest(http.MethodPatch, c.baseURL + "/me", bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	req.AddCookie(cookie)
	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	// Decode response.
	if rsp.StatusCode == http.StatusOK {
		resp := model.User{}
		rspBody, err := ioutil.ReadAll(rsp.Body)
		if err != nil {
			return nil, err
		}
		defer rsp.Body.Close()

		if err = json.Unmarshal(rspBody, &resp); err != nil {
			return nil, err
		}

		return &resp, nil
	}

	return nil, BuildErrorFromErrMsg(rsp)
}


// Login invokes the login method and returns a user if successful and an error otherwise.
func (c *RESTClient) Logout(cookie *http.Cookie) (*http.Cookie, error) {
	req, err := http.NewRequest("POST", c.baseURL + "/auth/logout", nil)
	if err != nil {
		return nil, err
	}
	req.AddCookie(cookie)
	rsp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	// Decode response.
	if rsp.StatusCode != http.StatusNoContent {
		return nil, BuildErrorFromErrMsg(rsp)
	}
	for _, cookie := range rsp.Cookies() {
		if cookie.Name == "roli_session" {
			return cookie, nil
		}
	}

	return nil, errors.New("missing cookie override")
}


func BuildErrorFromErrMsg(response *http.Response) *ErrorClient {
	errorMessage := serverUtils.ErrResp{}
	rspBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return &ErrorClient{err.Error(), 0}
	}
	defer response.Body.Close()
	if err = json.Unmarshal(rspBody, &errorMessage); err != nil {
		return &ErrorClient{err.Error(), 0}
	}

	return &ErrorClient{errorMessage.Message, response.StatusCode}
}
