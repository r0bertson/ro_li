package db_test

import (
	"bitbucket.org/r0bertson/ro_li/backend/db"
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"context"
	"database/sql"
	"github.com/rs/zerolog/log"
	migrate "github.com/rubenv/sql-migrate"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"os"
	"strings"
	"testing"
)

var mysql *db.MySQLClient
const testDBName = "roli_test_db"
const options = "?parseTime=true" //required to run migrations

type CanExec interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
}

// InitTestDB does setup for the test database.
func InitTestDB() string {
	var err error
	conn := os.Getenv("ROLI_TEST_DB")

	dbtmp, err := sql.Open("mysql", conn)
	if err != nil {
		log.Fatal().Err(err).Msg("opening test database")
	}
	defer dbtmp.Close()
	ResetSchema(dbtmp)

	return conn + testDBName + options
}

func ResetSchema(db *sql.DB) {
	MultiExec(db, resetDatabase)
}

func MultiExec(db CanExec, query string) {
	cleanQuery := strings.ReplaceAll(query, "\r", "")
	stmts := strings.Split(cleanQuery, ";\n")
	if len(strings.Trim(stmts[len(stmts)-1], " \n\t\r")) == 0 {
		stmts = stmts[:len(stmts)-1]
	}
	for _, s := range stmts {
		if _, err := db.Exec(s); err != nil {
			log.Fatal().Msgf("executing '%s': %s", s, err.Error())
		}
	}
}


var resetDatabase = `DROP DATABASE IF EXISTS roli_test_db;
CREATE DATABASE roli_test_db;
`

func init() {
	pgdsn := InitTestDB()
	var err error
	mysql, err = db.NewMySQLClient(context.Background(), pgdsn)
	if err != nil {
		log.Fatal().Err(err).Msg("couldn't connect to test database")
	}
}

func RunWithSchema(t *testing.T, test func(db *db.MySQLClient, t *testing.T)) {
	defer func() {
		ResetSchema(mysql.DB)
	}()

	migrations := &migrate.AssetMigrationSource{
		Asset:    db.Asset,
		AssetDir: db.AssetDir,
		Dir:      "migrations",
	}
	_, err := migrate.Exec(mysql.DB, "mysql", migrations, migrate.Up)
	assert.Nil(t, err, "database migrations failed!")

	test(mysql, t)
}

func loadDefaultFixture(db *db.MySQLClient, t *testing.T) {
	f, err := os.Open("test_fixture.sql")
	assert.Nil(t, err)
	defer f.Close()
	fixture, err := ioutil.ReadAll(f)
	assert.Nil(t, err)
	tx, _ := db.DB.Begin()
	MultiExec(tx, string(fixture))
	tx.Commit()
}

func TestROLIDatabase(t *testing.T) {
	RunWithSchema(t, func(mysql *db.MySQLClient, t *testing.T) {
		loadDefaultFixture(mysql, t)

		t.Run("TEST SESSIONS", func(t *testing.T) {
		// Look up existing session.
		session := mysql.LookupSession("TEST-SESSION-1")
		assert.NotNil(t, session)
		assert.Equal(t, "TEST-SESSION-1", session.Token)


		// Look up invalid session.
		session = mysql.LookupSession("SESSION-X")
		assert.NotNil(t, session)


		// Create and look up session.
		id, err := mysql.CreateSession("usr_TESTUSER4", "user4@domain.com")
		assert.Nil(t, err)
		session = mysql.LookupSession(id)
		assert.Equal(t, "usr_TESTUSER4",  session.UserID,)
		assert.Equal(t, "user4@domain.com", session.Email, )

		// Delete single session.
		session = mysql.LookupSession("SESSION-DELETE")
		assert.NotNil(t, session)
		err = mysql.DeleteSession("SESSION-DELETE")
		assert.Nil(t, err)
		session = mysql.LookupSession("SESSION-DELETE")
		//we expect a blank session
		assert.Equal(t, "", session.Token)

		})

		t.Run("TEST USERS", func(t *testing.T) {
			//RunWithSchema(t, func(mysql *db.MySQLClient, t *testing.T) {
			//	loadDefaultFixture(mysql, t)

				// Look up existing session.
				pwdRaw := "1234567890"

				//test hashing passswords
				pdwHashed, err := utils.HashAndSalt(pwdRaw)
				assert.Nil(t, err)
				request := model.ReqCreateUser{
					Type:       "native",
					Username:   "email@email.com",
					Password:   &pdwHashed,
					ExternalID: nil,
				}

				//test create user
				usr, err := mysql.CreateUser(&request)
				assert.Nil(t, err)
				assert.Equal(t, request.Username, usr.Username)


				//test create user with email already registered
				usr, err = mysql.CreateUser(&request)
				assert.Nil(t, usr)
				assert.NotNil(t, err)
				assert.Equal(t, db.ErrEmailAlreadyTaken, err)

				//test get known user
				usr, err = mysql.UserByID("usr_123456789")
				assert.Nil(t, err)
				assert.Equal(t, "usr_123456789", usr.ID)

				//test get invalid user
				usr, err = mysql.UserByID("invalid")
				assert.Nil(t, usr)
				assert.Equal(t, db.ErrUserNotFound, err)

				requestTwo := model.ReqCreateUser{
					Type:       "native",
					Username:   "emailtwo@email.com",
					Password:   &pwdRaw,
					ExternalID: nil,
				}
				// Create user and update it

				usrBefore, err := mysql.CreateUser(&requestTwo)
				assert.Nil(t, err)
				assert.Equal(t, requestTwo.Username, usrBefore.Username)

				usrBefore.FullName = sql.NullString{"update full name", true}

				err = mysql.UpdateUser(usrBefore)
				assert.Nil(t, err)

				usrAfter, err := mysql.UserByUsername(usrBefore.Username)
				assert.Nil(t, err)
				assert.Equal(t, usrBefore.FullName, usrAfter.FullName)
			})


	})
}

