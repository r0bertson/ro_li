package db

import (

	"database/sql"
	_ "github.com/go-sql-driver/mysql"

	"context"
	"github.com/pkg/errors"
	"github.com/rs/zerolog/log"
	migrate "github.com/rubenv/sql-migrate"
)


type assetFunc func(name string) ([]byte, error)
type assetDirFunc func(name string) ([]string, error)

type MySQLClient struct {
	DB *sql.DB
}

func NewMySQLClient(ctx context.Context, dbURL string) (*MySQLClient, error) {
	db, err := Connect(ctx, "ro_li", dbURL, Asset, AssetDir)
	if err != nil {
		return nil, err
	}
	return &MySQLClient{db}, nil
}

func Connect(ctx context.Context, dbName string, dbURL string, asset assetFunc, assetDir assetDirFunc) (*sql.DB, error) {
	// Connect to database and test connection integrity.
	db, err := sql.Open("mysql", dbURL)
	if err != nil {
		return nil, errors.Wrap(err, "opening " + dbName + " database")
	}

	if err = db.Ping(); err != nil {
		return nil, errors.Wrap(err, "pinging " + dbName + " database")
	}

	// Limit maximum connections
	db.SetMaxOpenConns(10)

	// Run and log database migrations.
	migrations := &migrate.AssetMigrationSource{
		Asset:    asset,
		AssetDir: assetDir,
		Dir:      "migrations",
	}
	n, err := migrate.Exec(db, "mysql", migrations, migrate.Up)
	if err != nil {
		log.Fatal().Err(err).Msg("database migrations failed!")
	}
	log.Info().Msgf("applied new database migrations: %d", n)
	migrationRecords, err := migrate.GetMigrationRecords(db, "mysql")
	if err != nil {
		log.Fatal().Err(err).Msg("couldn't read back database migration records")
	}
	if len(migrationRecords) == 0 {
		log.Info().Msg("no database migrations currently applied")
	} else {
		for _, m := range migrationRecords {
			log.Info().
				Str("migration", m.Id).
				Time("applied_at", m.AppliedAt).
				Msg("database migration")
		}
	}

	return db, nil
}