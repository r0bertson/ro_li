package db

import (
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"errors"
)

var ErrSessionNotFound = errors.New("session not found")
var ErrResetTokenNotFound = errors.New("reset token not found")
var ErrEmailAlreadyTaken = errors.New("email already taken")
var ErrUserNotFound = errors.New("user not found")

type DB interface {
	CreateSession(userID string, userEmail string) (string, error)
	LookupSession(token string) *model.Session
	DeleteSession(token string) error

	CreatePasswordResetToken(userID string) (string, error)
	LookupPasswordResetToken(token string) *model.PasswordReset
	DeletePasswordResetToken(token string) error

	CreateUser(req *model.ReqCreateUser) (*model.User, error)
	UserByUsername(username string) (*model.User, error)
	UserByID(id string) (*model.User, error)
	UpdateUser(u *model.User) error
	UpdatePassword(id, pwd string) error

}

//go:generate go-bindata -pkg db -o migrations.go migrations/...
//go:generate mockery --name=DB --output=../mocks
