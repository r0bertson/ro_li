package db

import (
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"database/sql"
	"time"
)

// CreateSession generates a new session token and stores it along with the associated user information.
func (msql *MySQLClient) CreateSession(userID string, userEmail string) (string, error) {
	id := NewID(20)

	if 	_, err := msql.DB.Exec(`INSERT INTO sessions VALUES (?, ?, ?)`, id, userID, userEmail); err != nil {
		return "", nil
	}

	return id, nil
}

// LookupSession checks a session token and returns user info if the session is known.
func (msql *MySQLClient) LookupSession(token string) *model.Session {
	s := model.Session{}
    if err := msql.DB.QueryRow(qLookupSession, token).Scan(&s.Token, &s.UserID, &s.Email); err != nil && err != sql.ErrNoRows {
		return nil
	}
	return &s
}

const qLookupSession = `
SELECT token, user_id, email
  FROM sessions
 WHERE token = ? `


// DeleteSession deletes a single session, i.e. logs a user out of their current session.
func (msql *MySQLClient) DeleteSession(token string) error {
	result, err := msql.DB.Exec(`DELETE FROM sessions WHERE token = ?`, token)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		return ErrSessionNotFound
	}
	return nil
}


// CreateSession generates a new session token and stores it along with the associated user information.
func (msql *MySQLClient) CreatePasswordResetToken(userID string) (string, error) {
	id := NewID(64)
	expiresAt := time.Now().Add(10 * time.Minute)
	if 	_, err := msql.DB.Exec(`INSERT INTO pwd_reset_tokens VALUES (?, ?, ?)`, id, userID, expiresAt); err != nil {
		return "", err
	}

	return id, nil
}

const qLookupPwdResetToken = `
SELECT token, user_id, expires_at
  FROM pwd_reset_tokens
 WHERE token = ? `

// LookupSession checks a session token and returns user info if the session is known.
func (msql *MySQLClient) LookupPasswordResetToken(token string) *model.PasswordReset {
	s := model.PasswordReset{}
	if err := msql.DB.QueryRow(qLookupPwdResetToken, token).Scan(&s.Token, &s.UserID, &s.ExpiresAt); err != nil {
		return nil
	}
	return &s
}

// DeleteSession deletes a single session, i.e. logs a user out of their current session.
func (msql *MySQLClient) DeletePasswordResetToken(token string) error {
	result, err := msql.DB.Exec(`DELETE FROM pwd_reset_tokens WHERE token = ?`, token)
	if err != nil {
		return err
	}
	rows, err := result.RowsAffected()
	if err != nil {
		return err
	}
	if rows != 1 {
		return ErrResetTokenNotFound
	}
	return nil
}
