-- +migrate Up

CREATE TABLE users (
    id VARCHAR(20) PRIMARY KEY NOT NULL,
    username VARCHAR(320) NOT NULL UNIQUE,
    type VARCHAR(20) NOT NULL DEFAULT 'native',
    password VARCHAR(256) NULL,
    external_id VARCHAR(64) NULL,
    full_name VARCHAR(256) NULL,
    address VARCHAR(256) NULL,
    telephone VARCHAR(20) NULL,
    is_setup BOOLEAN NOT NULL DEFAULT true
);

-- +migrate Down
DROP TABLE users;
