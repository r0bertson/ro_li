-- +migrate Up

CREATE TABLE pwd_reset_tokens
(
    token      VARCHAR(64) PRIMARY KEY,
    user_id     VARCHAR(20) NOT NULL REFERENCES users (id) ON DELETE CASCADE,
    expires_at DATETIME    NOT NULL DEFAULT NOW()
);
-- +migrate Down
DROP TABLE pwd_reset_tokens;

