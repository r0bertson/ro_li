-- +migrate Up

CREATE TABLE sessions
(
    token    VARCHAR(32) PRIMARY KEY,
    user_id  VARCHAR(24)  NOT NULL REFERENCES users(id) ON DELETE CASCADE,
    email    VARCHAR(320) NOT NULL
);


-- +migrate Down
DROP TABLE sessions;
