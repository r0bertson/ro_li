package db

import (
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"database/sql"
	"fmt"
)

const qCreateUser = `
INSERT INTO users 
    (id, type, username, password, external_id)
    VALUES (?, ?, ?, ?, ?) `


const qCheckEmailWhere = `SELECT COUNT(*) > 0 FROM users WHERE `


func (msql *MySQLClient) CreateUser(req *model.ReqCreateUser) (*model.User, error) {
	tx, err := msql.DB.Begin()
	if err != nil {
		return nil, err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()

	check := false
	if err = tx.QueryRow(qCheckEmailWhere + "username = ?", req.Username).Scan(&check); err != nil {
		return nil, err
	}

	if check {
		return nil, ErrEmailAlreadyTaken
	}

	idt := model.User{
		ID:         NewIDWithPrefix("usr"),
		Type:       req.Type,
		Username:   req.Username,
	}

	switch idt.Type {
	case "native":
		idt.Password = sql.NullString{String: *req.Password, Valid: true}
		idt.ExternalID = sql.NullString{Valid: false}
	case "google":
		idt.Password = sql.NullString{Valid: false}
		idt.ExternalID = sql.NullString{String: *req.ExternalID, Valid: req.ExternalID != nil}
	}

	if _, err = tx.Exec(qCreateUser, idt.ID, idt.Type, idt.Username, idt.Password, idt.ExternalID); err != nil {
		return nil, err
	}

	return &idt, nil
}


const qGetUserBy =
	`SELECT id, username, type, password, external_id, 
			full_name, address, telephone, is_setup 
	 FROM users WHERE `


func (msql *MySQLClient) UserByUsername(username string) (*model.User, error) {
	return msql.getUser(fmt.Sprintf(`username = '%s'`, username))
}

func (msql *MySQLClient) UserByID(id string) (*model.User, error) {
	return msql.getUser(fmt.Sprintf(`id = '%s'`, id))
}

func (msql *MySQLClient) getUser(whereParams string) (*model.User, error) {
	u := model.User{}
	q := qGetUserBy + whereParams
	if err := msql.DB.QueryRow(q).
		Scan(&u.ID, &u.Username, &u.Type, &u.Password, &u.ExternalID, &u.FullName,
			&u.Address, &u.Telephone, &u.IsSetup); err != nil {
		if err == sql.ErrNoRows {
			return nil, ErrUserNotFound
		}
		return nil, err
	}
	return &u, nil
}


const qUpdateUser = `
	UPDATE users 
	SET username = ?, full_name = ?, address = ?, telephone = ?, is_setup = false
	WHERE id = ? `

func (msql *MySQLClient) UpdateUser(u *model.User) error {
	tx, err := msql.DB.Begin()
	if err != nil {
		return err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()

	check := false
	//here we check by id because username can be changed
	if err = tx.QueryRow(qCheckEmailWhere + "id = ?", u.ID).Scan(&check); err != nil {
		return err
	}

	if !check {
		return ErrUserNotFound
	}

	if _, err = tx.Exec(qUpdateUser, u.Username, u.FullName, u.Address, u.Telephone, u.ID); err != nil {
		return err
	}
	u.IsSetup = false
	return nil
}


const qUpdatePassword = `UPDATE users SET password = ? WHERE id = ? `

func (msql *MySQLClient) UpdatePassword(id, pwd string) error {
	tx, err := msql.DB.Begin()
	if err != nil {
		return err
	}
	defer func() {
		switch err {
		case nil:
			err = tx.Commit()
		default:
			tx.Rollback()
		}
	}()


	if _, err = tx.Exec(qUpdatePassword, pwd, id); err != nil {
		return err
	}

	return nil
}