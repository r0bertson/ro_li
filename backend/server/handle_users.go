package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/db"
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

func (s *Server) getUser(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	auth := s.getAuthentication(r)
	if auth == nil {
		return utils.Unauthorized(w, "user not authenticated")
	}

	usr, err := s.db.UserByID(auth.UserID)
	if err != nil {
		if err == db.ErrUserNotFound {
			return utils.NotFoundWithMessage(w, "user not found")
		}
		return nil, err
	}

	return usr.GetPublicView(), nil
}

func (s *Server) createUser(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	// Decode request body
	body, err := utils.ReadBody(r)
	if err != nil {
		return utils.BadRequest(w, err.Error())
	}

	var req model.ReqCreateUser
	if err = json.Unmarshal(body, &req); err != nil {
		return utils.BadRequest(w, err.Error())
	}

	usr, err:= s.db.UserByUsername(req.Username)
	if err != nil && err != db.ErrUserNotFound{
		return nil, err
	}

	if usr != nil {
		return utils.BadRequest(w, "user already registered")
	}

	if req.Password == nil || *req.Password == "" {
		return utils.BadRequest(w, "password cannot be blank")
	}

	//encrypt password before storing
	hashedPwd, err := utils.HashAndSalt(*req.Password)
	if err != nil {
		return nil, err

	}
	req.Password = &hashedPwd //replace clear password with hash

	idt, err := s.db.CreateUser(&req)
	if err != nil {
		if err == db.ErrEmailAlreadyTaken {
			return utils.BadRequest(w, err.Error())
		}
		return nil, err
	}

	return idt, nil

}
func (s *Server) getGoogleProfile(r *http.Request) (*model.GoogleProfile, error) {

	body, err := utils.ReadBody(r)
	if err != nil {
		return nil, err
	}

	var req model.ReqGoogleAuth
	if err = json.Unmarshal(body, &req); err != nil {
		return nil, err
	}
	//decode code into token
	tok, err := s.GoogleOAuth.Exchange(context.TODO(), req.Code)
	if err != nil {
		return nil, err
	}

	//get user's google profile
	c := s.GoogleOAuth.Client(context.TODO(), tok)
	email, err := c.Get("https://www.googleapis.com/oauth2/v3/userinfo")
	if err != nil {
		return nil, err
	}
	defer email.Body.Close()
	data, _ := ioutil.ReadAll(email.Body)

	profile := model.GoogleProfile{}
	//parse response into struct
	if err = json.Unmarshal(data, &profile); err != nil {
		return nil, err
	}

	return &profile, nil
}

func (s *Server) handleGoogleUser(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	profile, err := s.getGoogleProfile(r)
	if err != nil {
		return utils.BadRequest(w, err.Error())
	}

	create := model.ReqCreateUser{
		Type:       "google",
		Username:   profile.Email,
		ExternalID: &profile.Sub,
	}

	idt, err := s.db.CreateUser(&create)
	if err != nil {
		if err == db.ErrEmailAlreadyTaken {
			return utils.BadRequest(w, err.Error())
		}
		return nil, err
	}

	return idt, nil
}

func (s *Server) updateUser(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	auth := s.getAuthentication(r)
	if auth == nil {
		return utils.Unauthorized(w, "user not authenticated")
	}

	usr, err := s.db.UserByID(auth.UserID)
	if err != nil {
		if err == db.ErrUserNotFound {
			return utils.NotFoundWithMessage(w, "user not found")
		}
		return nil, err
	}

	usernameBefore := usr.Username

	// Read patch request body.
	body, err := utils.ReadBody(r)
	if err != nil {
		return utils.BadRequest(w, err.Error())
	}

	//patching the user struct
	if err = usr.Patch(body); err != nil {
		return utils.BadRequest(w, err.Error())
	}

	if usr.Type == "google" && usr.Username != usernameBefore{
		return utils.BadRequest(w, "cannot update username/email on google accounts")
	}

	// Do the update.
	if err = s.db.UpdateUser(usr); err != nil {
		if err == db.ErrUserNotFound {
			return utils.NotFoundWithMessage(w, "user not found")
		}
		return nil, err
	}

	return usr, nil
}

