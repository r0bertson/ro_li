package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"github.com/go-chi/chi"

)

func (s *Server) routes() chi.Router {
	r := chi.NewRouter()
	r.Group(func (r chi.Router){

		r.Get("/", utils.CommonHandler(utils.SimpleHealth))
		r.Get("/healthz",  utils.CommonHandler(utils.SimpleHealth))

		r.Post("/auth/login", utils.CommonHandler(s.loginNative))
		r.Post("/auth/google", utils.CommonHandler(s.handleGoogle))
		r.Post("/auth/logout", utils.CommonHandler(s.logout))

		r.Post("/users", utils.CommonHandler(s.createUser))

		r.Get("/me", utils.CommonHandler(s.getUser))
		r.Patch("/me", utils.CommonHandler(s.updateUser))

		r.Post("/request-password-reset", utils.CommonHandler(s.createPasswordRequest))
		r.Post("/password-reset", utils.CommonHandler(s.resetPassword))
		r.Post("/auth/logout", utils.CommonHandler(s.logout))

	})

	return r
}
