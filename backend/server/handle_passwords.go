package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/db"
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"bitbucket.org/r0bertson/ro_li/backend/templates"
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"time"
)

func (s *Server) createPasswordRequest(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	// Decode request body to get email
	username, ok := r.URL.Query()["username"]
	if !ok || len(username[0]) == 0 {
		return utils.BadRequest(w, "missing username param")
	}

	usr, err := s.db.UserByUsername(username[0])
	if err != nil {
		if err == db.ErrUserNotFound {
			// although the system is not sending any email, it'll return a success to the front
			// this should prevent user enumeration attacks
			return utils.NoContent(w)
		}
		return nil, err
	}

	if usr.Type != "native" {
		return utils.BadRequest(w, "sign in method does not allow password changes")
	}

	token, err := s.db.CreatePasswordResetToken(usr.ID)
	if err != nil {
		//500 Internal Server Error
		return nil, errors.New("reset token couldn't be created")
	}

	emailData := map[string]interface{}{}
	emailData["User"] = usr.FullName
	emailData["Link"] = fmt.Sprintf("%s?token=%s",s.resetPasswordURL, token)

	//build message from template and input data
	message, err := parseTemplate(templates.ResetPasswordTemplate, emailData)
	if err != nil {
		return nil, err
	}
	//send message only when running live mode
	//TODO: make smtp server mockable so we can run tests without sending emails
	if !s.isDevMode {
		if err = s.smtpServer.Send([]string{usr.Username},"Password reset", message); err != nil {
			return nil, errors.New("server couldn't send email with reset link")
		}
	}

	return utils.NoContent(w)
}

func parseTemplate(t string, data map[string]interface{}) ([]byte, error) {
	tmpl, err := template.New("password_reset").Parse(t)
	if err != nil {
		return nil, err
	}
	var message bytes.Buffer
	if err = tmpl.Execute(&message, data); err != nil {
		return nil, err
	}
	return message.Bytes(), nil
}


func (s *Server) resetPassword(w http.ResponseWriter, r *http.Request) (interface{}, error) {

	// Decode request body to get email and password.
	body, err := utils.ReadBody(r)
	if err != nil {
		return utils.BadRequest(w, err.Error())
	}
	var req model.ReqResetPassword
	if err = json.Unmarshal(body, &req); err != nil {
		return utils.BadRequest(w, err.Error())
	}

	token := s.db.LookupPasswordResetToken(req.Token)
	if token == nil || token.ExpiresAt.Before(time.Now()) {
		return utils.BadRequest(w, "reset token is invalid or expired")
	}

	usr, err := s.db.UserByID(token.UserID)
	if err != nil {
		if err == db.ErrUserNotFound {
			return utils.BadRequest(w, err.Error())
		}
		return nil, err
	}

	hashedPwd, err := utils.HashAndSalt(req.Password)
	if err != nil {
		return nil, err

	}
	usr.Password = sql.NullString{hashedPwd, true}

	if err = s.db.UpdatePassword(usr.ID, usr.Password.String); err != nil {
		return nil, err
	}

	//if token was not found, something must already cleared it
	//probably the scheduled event on the database
	//see migrations/0003.password-reset.sql to check the code
	if err = s.db.DeletePasswordResetToken(token.Token); err != nil && err != db.ErrResetTokenNotFound {
		//TODO: may be worth to add a retry mechanism (probably handling it on a separate go routine)
		//   in case deletion fails due to any other reason besides 'token not found'
	}

	return utils.NoContent(w)
}