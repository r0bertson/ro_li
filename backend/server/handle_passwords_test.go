package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/db"
	"bitbucket.org/r0bertson/ro_li/backend/mocks"
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"database/sql"
	"github.com/gavv/httpexpect"
	"github.com/stretchr/testify/mock"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var (
	testUser1 = model.User{
		ID:         "usr_testuser",
		Username:   "user@email.com",
		Type:       "native",
		Password:   sql.NullString{},
		ExternalID: sql.NullString{},
		FullName:   sql.NullString{},
		Address:    sql.NullString{},
		Telephone:  sql.NullString{},
		IsSetup:    false,
	}
	testUser2 = model.User{
		ID:         "usr_testgoogle",
		Username:   "google@email.com",
		Type:       "google",
		Password:   sql.NullString{},
		ExternalID: sql.NullString{},
		FullName:   sql.NullString{},
		Address:    sql.NullString{},
		Telephone:  sql.NullString{},
		IsSetup:    false,
	}

	tokenOk = "randomtoken1234567"
	tokenOK2 = "otherrandomtoken123"
	tokenDeleted = "deleted123456789"
	pwdResetTokenOK = model.PasswordReset{
		Token:     tokenOk,
		UserID:    testUser1.ID,
		ExpiresAt: time.Now().Add(10*time.Hour),
	}
	pwdResetTokenExpired = model.PasswordReset{
		Token:     tokenOk,
		UserID:    testUser1.ID,
		ExpiresAt: time.Now().Add(-10*time.Hour),
	}
)
var dbMock = mocks.DB{}

func RunWithServer(t *testing.T, test func(e *httpexpect.Expect)) {
	s := &Server{}
	s.Init("test-backend", 8090, s.routes())
	s.isDevMode = true
	dbMock = mocks.DB{}
	s.db = &dbMock

	srv := httptest.NewServer(s.Srv.Handler)
	defer srv.Close()

	e := httpexpect.New(t, srv.URL)

	test(e)
}

func TestRequestPasswordReset(t *testing.T) {
	RunWithServer(t, func(e *httpexpect.Expect) {
		dbMock.
			On("UserByUsername", "invalid@email.com").
			Return(nil, db.ErrUserNotFound)
		dbMock.
			On("UserByUsername", "user@email.com").
			Return(&testUser1, nil)
		dbMock.
			On("UserByUsername", "google@email.com").
			Return(&testUser2, nil)
		dbMock.
			On("CreatePasswordResetToken", "usr_testuser").
			Return(tokenOk, nil)

		//REQUEST TO VALID USER
		e.POST("/request-password-reset").WithQuery("username", "user@email.com").
			Expect().
			Status(http.StatusNoContent)

		//REQUEST TO INVALID USER (RETURN SUCCESS AS SECURITY MEASURE TO AVOID A KIND OF ATTACK)
		e.POST("/request-password-reset").WithQuery("username", "invalid@email.com").
			Expect().
			Status(http.StatusNoContent)

		// REQUEST TO VALID USER, BUT GOOGLE ACCOUNT
		e.POST("/request-password-reset").WithQuery("username", "google@email.com").
			Expect().
			Status(http.StatusBadRequest)
	})
}


func TestPasswordResetAction(t *testing.T) {
	RunWithServer(t, func(e *httpexpect.Expect) {
		dbMock.
			On("LookupPasswordResetToken", tokenOk).
			Return(&pwdResetTokenOK, nil)
		dbMock.
			On("LookupPasswordResetToken", tokenOK2).
			Return(&pwdResetTokenExpired, nil)
		dbMock.
			On("LookupPasswordResetToken", tokenDeleted).
			Return(nil, db.ErrResetTokenNotFound)
		dbMock.
			On("UserByID",testUser1.ID ).
			Return(&testUser1, nil)
		dbMock.
			On("UpdateUser", mock.Anything).
			Return(nil)
		dbMock.
			On("DeletePasswordResetToken", mock.Anything).
			Return(nil)

		//TEST VALID RESET CALL
		e.POST("/password-reset").
			WithJSON(map[string]string{"token": tokenOk, "password": "123456789"}).
			Expect().
			Status(http.StatusNoContent)

		//TEST INVALID RESET CALL (TOKEN EXPIRED)
		e.POST("/password-reset").
			WithJSON(map[string]string{"token": tokenOK2, "password": "123456789"}).
			Expect().
			Status(http.StatusBadRequest)

		//TEST INVALID RESET CALL (TOKEN NOT EXISTS)
		e.POST("/password-reset").
			WithJSON(map[string]string{"token": tokenOK2, "password": "123456789"}).
			Expect().
			Status(http.StatusBadRequest)

	})
}

