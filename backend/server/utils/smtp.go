package utils

import (
	"net/smtp"
)


// SMTPServer config.
type SMTPServer struct {
	Auth smtp.Auth
	Address string
	From string
}

func NewSMTPServer(host, port, from, password string) *SMTPServer {
	return &SMTPServer{
		Auth: smtp.PlainAuth("", from, password, host),
		Address: host + ":" + port,
		From:     from,
	}
}

func (s *SMTPServer) Send( to []string, subject string, message []byte) error {

	mime := "MIME-version: 1.0;\nContent-Type: text/plain; charset=\"UTF-8\";\n\n"
	subject = "Subject: " + subject + "\n"

	message = append([]byte(subject+ mime), message...)

	return smtp.SendMail(s.Address, s.Auth, s.From, to, message)
}