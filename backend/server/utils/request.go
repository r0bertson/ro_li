package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/rs/zerolog/log"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
)
// CommonHandlerFunc is a HTTP handler function that signals internal
// errors by returning a normal Go error, and when successful returns
// a response body to be marshalled to JSON. It can be wrapped in the
// SimpleHandler middleware to produce a normal HTTP handler function.
type CommonHandlerFunc func(w http.ResponseWriter, r *http.Request) (interface{}, error)

func CommonHandler(inner CommonHandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		//Setting common headers. This could be done by creating a middleware on chi
		w.Header().Set("Access-Control-Allow-Origin", "*") //TODO: enable CORS middleware for production
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, PATCH, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type")
		w.Header().Set("Access-Control-Allow-Credentials", "true")

		// Run internal handler: obtains a marshalable result and an
		// error, either of which may be nil. The combination of the returned
		// result is going to decide the type of http response code this handler
		// will return
		result, err := inner(w, r)

		// All known behavior is handled on inner's CommonHandlerFunc.
		// Any error returned to this handler will be treated as a "500 Internal Server Error".
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Error().Err(err).Msgf("handling %q", r.RequestURI)
			return
		}

		// When both result and err are null, it means that the inner CommonHandlerFunc
		// already dealt with the request response setup.
		if result == nil {
			return
		}

		// When result is present, we'll marshal it as JSON and return as '200 Success'.
		body, err := json.Marshal(result)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.Error().Err(err).Msgf("handling %q", r.RequestURI)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(body)
	}
}



const defaultMaxBodySize = 10 * uint64(1) << 10 << 10 //10 MB

var ErrInvalidRequestBody = "invalid request body"
// ReadBody reads a request body limiting to 10mb payload
func ReadBody(r *http.Request) ([]byte, error) {
	limiter := &io.LimitedReader{R: r.Body, N: int64(defaultMaxBodySize)}
	data, err := ioutil.ReadAll(limiter)
	defer r.Body.Close()
	if err != nil {
		return nil, errors.New(ErrInvalidRequestBody)
	}
	if limiter.N <= 0 {
		return nil, errors.New(fmt.Sprintf("Request body too large (limit is %s)",
			strconv.FormatUint(defaultMaxBodySize, 10)))
	}
	return data, nil
}


func SimpleHealth(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	w.Write([]byte("OK"))
	return nil, nil
}
