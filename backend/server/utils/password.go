package utils

import "golang.org/x/crypto/bcrypt"

func HashAndSalt(key string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(key), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hash), nil
}

func CompareHashedKeys(hashedKey, plainKey string) bool {
	if err := bcrypt.CompareHashAndPassword([]byte(hashedKey), []byte(plainKey)); err != nil {
		return false
	}
	return true
}
