package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/db"
	"bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

// Config contains the configuration information needed to start the blob service.
type Config struct {
	AppName                string
	DevMode                bool   `env:"DEV_MODE,default=false"`
	DBURL                  string `env:"DATABASE_URL,required"`
	Port                   int    `env:"PORT,default=2000"`
	GoogleOAuthClientID    string `env:"GOOGLE_OAUTH_CLIENT_ID,required"`
	GoogleOAuthSecret      string `env:"GOOGLE_OAUTH_SECRET,required"`
	GoogleOAuthRedirectURL string `env:"GOOGLE_OAUTH_REDIRECT_URL,required"`
	SessionMaxAge          int    `env:"SESSION_MAX_AGE,default=216000"`
	UseHTTPS               bool   `env:"USE_HTTPS,default=true"`

	SMTPHost     string `env:"SMTP_HOST,required"`
	SMTPPort     string `env:"SMTP_PORT,required"`
	SMTPUser     string `env:"SMTP_USER,required"`
	SMTPPassword string `env:"SMTP_PASSWORD,required"`

	ResetPasswordURL string `env:"RESET_PASSWORD_URL,required"`
}

// Server is the server structure.
type Server struct {
	ApplicationName  string
	Srv              *http.Server
	Ctx              context.Context
	db               db.DB
	encKey           string
	GoogleOAuth      *oauth2.Config
	useHTTPS         bool
	sessionMaxAge    int
	smtpServer       *utils.SMTPServer
	resetPasswordURL string
	isDevMode        bool
}

// NewServer creates the server structure.
func NewServer(cfg *Config) *Server {
	var err error

	// Common server initialisation.
	s := &Server{
		useHTTPS:         cfg.UseHTTPS,
		sessionMaxAge:    cfg.SessionMaxAge,
		resetPasswordURL: cfg.ResetPasswordURL,
		isDevMode:        cfg.DevMode,
	}

	s.GoogleOAuth = &oauth2.Config{
		ClientID:     cfg.GoogleOAuthClientID,
		ClientSecret: cfg.GoogleOAuthSecret,
		RedirectURL:  cfg.GoogleOAuthRedirectURL,
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
		},
		Endpoint: google.Endpoint,
	}

	s.smtpServer = utils.NewSMTPServer(cfg.SMTPHost, cfg.SMTPPort, cfg.SMTPUser, cfg.SMTPPassword)

	/* DESIGN DECISION
	Here we are using a chi router instead of building using default http.HandleFunc.
	In a small project like this, the main effect of using chi router is to avoid code
	repetition validating http methods inside handlers. I won't be using any other
	functionality of chi router besides this one.
	*/
	s.Init(cfg.AppName, cfg.Port, s.routes())

	// Connect to  database.
	timeout, _ := context.WithTimeout(context.Background(), time.Second*10)

	s.db, err = db.NewMySQLClient(timeout, cfg.DBURL)
	if err != nil {
		log.Fatal().Err(err).Msg("couldn't connect to ro_li database")
	}

	return s
}

func (s *Server) Init(appName string, port int, router chi.Router) {
	rand.Seed(int64(time.Now().Nanosecond())) //used when generating random ids

	s.ApplicationName = appName
	s.Ctx = context.Background()
	s.Srv = &http.Server{
		// Handler: h,
		Handler: router,
		Addr:    fmt.Sprintf(":%d", port),
	}

}

// Serve runs a server event loop and performs shutdown when application is stopped.
func (s *Server) Serve() {
	errChan := make(chan error, 0)
	go func() {
		log.Info().Str("address", s.Srv.Addr).Msg("server started")
		if err := s.Srv.ListenAndServe(); err != nil {
			errChan <- err
		}
	}()

	signalCh := make(chan os.Signal, 0)
	signal.Notify(signalCh, os.Interrupt, syscall.SIGTERM)

	var err error

	select {
	case <-signalCh:
	case err = <-errChan:
	}
	//performs server shutdown
	ctx, cancel := context.WithTimeout(s.Ctx, 10*time.Second)
	defer cancel()
	if err := s.Srv.Shutdown(ctx); err != nil {
		log.Error().Err(err)
	}

	if err == nil {
		log.Info().Msg("server shutting down")
	} else {
		log.Fatal().Err(err).Msg("server failed")
	}
}
