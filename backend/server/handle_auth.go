package server

import (
	"bitbucket.org/r0bertson/ro_li/backend/db"
	"bitbucket.org/r0bertson/ro_li/backend/model"
	"bitbucket.org/r0bertson/ro_li/backend/server/utils"
	"encoding/json"
	"net/http"
	"time"
)

const sessionCookieName = "roli_session"

func (s *Server) getAuthentication(r *http.Request) *model.Session {
	if cookie, err := r.Cookie(sessionCookieName); err == nil {
		return s.db.LookupSession(cookie.Value)
	}
	return nil
}

func (s *Server) loginNative(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	// Decode request body to get email and password.
	body, err := utils.ReadBody(r)
	if err != nil {
		return utils.BadRequest(w, err.Error())
	}
	var req model.ReqLoginNative
	if err = json.Unmarshal(body, &req); err != nil {
		return utils.BadRequest(w, err.Error())
	}

	usr, err := s.db.UserByUsername(req.Username)
	if err != nil {
		if err == db.ErrUserNotFound {
			return utils.NotFoundWithMessage(w, err.Error())
		}
		return nil, err
	}

	if usr.Type != "native" {
		return utils.BadRequest(w, "incorrect sign in method")
	}

	if !utils.CompareHashedKeys(usr.Password.String, req.Password) {
		return utils.Unauthorized(w, "invalid password")
	}

	// Create a session for the user.
	token, err := s.db.CreateSession(usr.ID, usr.Username)
	if err != nil {
		return nil, err
	}

	// Set the session cookie and return the user information as a JSON response.
	auth := http.Cookie{
		Name:     sessionCookieName,
		Value:    token,
		MaxAge:   s.sessionMaxAge,
		Secure:   s.useHTTPS,
		HttpOnly: true,
		Path:     "/",
	}
	http.SetCookie(w, &auth)

	return model.RespLogin{UserID: usr.ID, Token: token}, nil
}

func (s *Server) handleGoogle(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	profile, err := s.getGoogleProfile(r)
	if err != nil {
		return utils.BadRequest(w, err.Error())
	}

	usr, err := s.db.UserByUsername(profile.Email)
	if err != nil && err != db.ErrUserNotFound {
		return nil, err
	}

	// We handle sign up and sign in for google auth in the same request, so
	//if user is not registered yet, we'll perform the registration before
	// returning its session cookie

	if usr == nil {
		createUserRequest := model.ReqCreateUser{
			Type:       "google",
			Username:   profile.Email,
			ExternalID: &profile.Sub,
		}
		usr, err = s.db.CreateUser(&createUserRequest)
		if err != nil {
			if err == db.ErrEmailAlreadyTaken {
				return utils.BadRequest(w, err.Error())
			}
			return nil, err
		}
	}

	if usr.Type != "google" {
		return utils.BadRequest(w, "email was registered with other sign up method")
	}
	// Create a session for the user.
	// no need for checking if usr == nil again, because if an error occurred
	// on user creation, the handler already returned
	token, err := s.db.CreateSession(usr.ID, usr.Username)
	if err != nil {
		return nil, err
	}

	// Set the session cookie and return the user information as a JSON response.
	auth := http.Cookie{
		Name:     sessionCookieName,
		Value:    token,
		MaxAge:   s.sessionMaxAge,
		Secure:   s.useHTTPS,
		HttpOnly: true,
		Path:     "/",
	}
	http.SetCookie(w, &auth)

	return model.RespLogin{UserID: usr.ID, Token: token}, nil
}

func (s *Server) logout(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	auth := s.getAuthentication(r)
	if auth == nil {
		return utils.NotFound(w)
	}

	// Delete single session from database.
	cookie, _ := r.Cookie(sessionCookieName)
	_ = s.db.DeleteSession(cookie.Value)
	s.clearSessionCookie(w)

	return utils.NoContent(w)
}

// Delete session cookie by setting expiry in past.
func (s *Server) clearSessionCookie(w http.ResponseWriter) {
	delAuth := http.Cookie{
		Name:     sessionCookieName,
		Value:    "",
		Secure:   s.useHTTPS,
		HttpOnly: true,
		Path:     "/",
		Expires:  time.Now().Add(-1 * time.Hour),
	}
	http.SetCookie(w, &delAuth)
}
