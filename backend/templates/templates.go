package templates

const ResetPasswordTemplate = `
Hello {{ .Name }},

a password reset has been requested for your ROLI account. Click on following link to reset your password:

{{ .Link }}

This link will expire in 10 minutes.
	
`