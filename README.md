# WELCOME TO RO_LI PROJECT

This project consists of two Go applications. One of them is a Back-end REST API that communicates with a MySQL database.
The other one is the Front-end using Go http/templates.

## How can I run it?
You need to set up a few credentials on your environment variables, such as SNMP host, Google OAuth Credentials and database URL. You can see a list of variables required by each application on `frontend/dev.env` or `backend/dev.env` files. After setting them up, just navigate to each application folder and `go run main.go`.

If you built both applications, remember that the frontend requires that the `/templates` folder and its content to be placed on the same path as the final binary.

> "That's a lot of work. Can I use docker?"

Yes! I added a docker-compose.yml and an `/envs` folder outside the applications folder. If you setup each of these `.env` files, you can run both applications and database with just one command:

```
docker-compose up

#If you want to run in background

docker-compose -d up

```

> "Can I see a live version?"

Yes! I deployed it on GCP. You can check it out on http://roli.limatech.xyz/, but keep in mind that I didn't set any certificate authority on this project's server, so you cannot use https. Its worth to tell you that the SNMP account used in this deployment is a free Gmail account and has a limit of 100 emails per day. Please trigger email requests with a certain concern about this number, because if this limit is reached, your requests will start to fail.

> "But your page is down..."

That seems unlikely, because this server has a fixed IP and if one application crashes, it restarts immediately. I don't have control about Cloudflare's DNS service, though. But you can send me an [email](mailto:email@robertsonlima.com) and I'll take a look. 